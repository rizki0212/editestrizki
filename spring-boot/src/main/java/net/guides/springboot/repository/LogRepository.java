package net.guides.springboot.repository;

import org.apache.commons.logging.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.guides.springboot.model.Employee;

@Repository
public interface LogRepository extends JpaRepository<Log, Long>{

}
