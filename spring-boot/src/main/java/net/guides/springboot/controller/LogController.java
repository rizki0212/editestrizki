package net.guides.springboot.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.guides.springboot.exception.ResourceNotFoundException;
import net.guides.springboot.model.Log;
import net.guides.springboot.repository.LogRepository;

@RestController
@RequestMapping("/api/v1")
public class LogController {
	
	@Autowired
	private LogRepository logRepository;

	@GetMapping("/log")
	public List<Log> getAllLog() {
		return logRepository.findAll();
	}

	@GetMapping("/log/{id}")
	public ResponseEntity<Log> getLogById(@PathVariable(value = "id") Long logId)
			throws ResourceNotFoundException {
		Log log = logRepository.findById(logId)
				.orElseThrow(() -> new ResourceNotFoundException("Log not found for this id :: " + logId));
		return ResponseEntity.ok().body(log);
	}

	@PostMapping("/log")
	public Log createLog(@Valid @RequestBody Log log) {
		return logRepository.save(log);
	}

	@PutMapping("/log/{id}")
	public ResponseEntity<Log> updateLog(@PathVariable(value = "id") Long logId,
			@Valid @RequestBody Log logDetails) throws ResourceNotFoundException {
		Log log = logRepository.findById(logId)
				.orElseThrow(() -> new ResourceNotFoundException("Log not found for this id :: " + logId));

		log.setEmailId(logDetails.getEmailId());
		log.setLastName(logDetails.getLastName());
		log.setFirstName(logDetails.getFirstName());
		final Log updatedLog = logRepository.save(log);
		return ResponseEntity.ok(updatedLog);
	}

	@DeleteMapping("/log/{id}")
	public Map<String, Boolean> deleteLog(@PathVariable(value = "id") Long logId)
			throws ResourceNotFoundException {
		Log log = logRepository.findById(logId)
				.orElseThrow(() -> new ResourceNotFoundException("Log not found for this id :: " + logId));

		logRepository.delete(log);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
