package net.guides.springboot.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.guides.springboot.exception.ResourceNotFoundException;
import net.guides.springboot.model.Image;
import net.guides.springboot.repository.ImageRepository;

@RestController
@RequestMapping("/api/v1")
public class ImageController {
	
	@Autowired
	private ImageRepository imageRepository;

	@GetMapping("/image")
	public List<Image> getAllImage() {
		return imageRepository.findAll();
	}

	@GetMapping("/image/{id}")
	public ResponseEntity<Image> getImageById(@PathVariable(value = "id") Long imageId)
			throws ResourceNotFoundException {
		Image image = imageRepository.findById(imageId)
				.orElseThrow(() -> new ResourceNotFoundException("Image not found for this id :: " + imageId));
		return ResponseEntity.ok().body(image);
	}

	@PostMapping("/image")
	public Image createImage(@Valid @RequestBody Image image) {
		return imageRepository.save(image);
	}

	@PutMapping("/image/{id}")
	public ResponseEntity<Image> updateImage(@PathVariable(value = "id") Long imageId,
			@Valid @RequestBody Image imageDetails) throws ResourceNotFoundException {
		Image image = imageRepository.findById(imageId)
				.orElseThrow(() -> new ResourceNotFoundException("Image not found for this id :: " + imageId));

		image.setEmailId(imageDetails.getEmailId());
		image.setLastName(imageDetails.getLastName());
		image.setFirstName(imageDetails.getFirstName());
		final Image updatedImage = imageRepository.save(image);
		return ResponseEntity.ok(updatedImage);
	}

	@DeleteMapping("/image/{id}")
	public Map<String, Boolean> deleteImage(@PathVariable(value = "id") Long imageId)
			throws ResourceNotFoundException {
		Image image = imageRepository.findById(imageId)
				.orElseThrow(() -> new ResourceNotFoundException("Image not found for this id :: " + imageId));

		imageRepository.delete(image);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}