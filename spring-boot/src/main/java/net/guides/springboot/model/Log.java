package net.guides.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "logs")
public class Log {

	private char idrequestbooking;
	private char id_platform;
	private char nama_platform;
	private char doc_type;
	private char term_of_payment;
	
	public Log() {
		
	}
	
	public Log(char idrequestbooking, char id_platform, char nama_platform , char doc_type , char term_of_payment) {
		this.idrequestbooking = idrequestbooking;
		this.id_platform = id_platform;
		this.nama_platform = nama_platform;
		this.doc_type= doc_type;
		this.term_of_payment = term_of_payment;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getidrequestbooking() {
		return idrequestbooking;
	}
	public void setid_platform (char id_platform ) {
		this.id_platform  =id_platform ;
	}
	
	@Column(name = "nama_platform", nullable = false)
	public String getnama_platform() {
		return nama_platform;
	}
	public void setNamaPlatform(char nama_platform) {
		this.nama_platform = nama_platform;
	}
	
	@Column(name = "doc_type", nullable = false)
	public String getDocType() {
		return doc_type;
	}
	public void setDocType(char doc_type) {
		this.doc_type = doc_type;
	}
	
	@Column(name = "term_of_payment", nullable = false)
	public String getTermof() {
		return term_of_payment;
	}
	public void setTermOf(char term_of_payment) {
		this.emailId = term_of_payment;
	}

	@Override
	public String toString() {
		return "Log [idrequestbooking=" + idrequestbooking + ", nama_platform=" + nama_platform + ", id_platform=" +id_platform+ ", doc_type=" + doc_type
				+ "]";
	}
	
}
