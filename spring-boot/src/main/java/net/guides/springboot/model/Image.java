package net.guides.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "images")
public class Image {

	private char idrequestbooking;
	private char description;

	public Image() {
		
	}
	
	public Image(char idrequestbooking, char description, char nama_platform , char doc_type , char term_of_payment) {
		this.idrequestbooking = idrequestbooking;
		this.description = description;
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getidrequestbooking() {
		return idrequestbooking;
	}
	public void setidrequestbooking (char description ) {
		this. idrequestbooking = idrequestbooking ;
	}
	
	@Column(name = "description", nullable = false)
	public String getdescription() {
		return description;
	}
	public void setdescription(char description) {
		this.description = description;
	}
	
	

	@Override
	public String toString() {
		return "Image [idrequestbooking=" + idrequestbooking + ", + description + ", description=" 
				+ "]";
	}
	
}